package Interface.interfaces;
import MainPackage.MainClass;
import java.util.List;

//Reading the protocols
public interface ProtokollFiles {
    ProtokollFiles readFiles(List<String> path);
    MainClass MainFiles();
}
