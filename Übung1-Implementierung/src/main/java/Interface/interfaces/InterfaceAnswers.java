package Interface.interfaces;

/**
 * Antworten werden in die Dateien mit einem anderen Interface geschrieben
 * @param <P> Pfad zur speichernden Datei
 * @param <C> Inhalt zum Speichern
 */
public interface InterfaceAnswers<P,C> {
    void ZuDateien(P path, C content);
}
