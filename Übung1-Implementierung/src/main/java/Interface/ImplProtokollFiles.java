package Interface;
import Interface.interfaces.ProtokollFiles;
import MainPackage.MainClass;
import dbt.protokoll.domain.DBT;
import org.xml.sax.*;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;

import java.util.ArrayList;
import java.util.List;


public class ImplProtokollFiles implements ProtokollFiles {

    private JAXBContext jaxbContext;
    private XMLReader xmlReader;
    private Unmarshaller jaxbUnmarshaller;
    private List<DBT> dbtplenarprotokolls;

    public ImplProtokollFiles() {
        try {
            jaxbContext = JAXBContext.newInstance(DBT.class);

            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            spf.setFeature("http://xml.org/sax/features/validation", false);
            spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            xmlReader = spf.newSAXParser().getXMLReader();
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private DBT readFile(String path) {

        try {
            System.out.println(String.format("Processing file: %s", path));
            // the stream holding the file content

            InputSource inputSource = new InputSource(path);
            SAXSource source = new SAXSource(xmlReader, inputSource);
            DBT dbtplenarprotokoll = (DBT) jaxbUnmarshaller.unmarshal(source);
            return dbtplenarprotokoll;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ProtokollFiles readFiles(List<String> paths) {
        List<DBT> dbtplenarprotokollList = new ArrayList<DBT>();

        for (String path : paths) {
            DBT dbtplenarprotokoll = readFile(path);
            if (dbtplenarprotokoll != null){
                dbtplenarprotokollList.add(dbtplenarprotokoll);
            }
        }
        this.dbtplenarprotokolls = dbtplenarprotokollList;
        return this;
    }

    @Override
    public MainClass MainFiles() {
        return new MainClass(dbtplenarprotokolls);
    }
}
