package Interface;

import Interface.interfaces.InterfaceAnswers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class ImplInterfaceAnswers implements InterfaceAnswers<String, String> {
    public void ZuDateien(String path, String content) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter(path));
            printWriter.print(content);
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
