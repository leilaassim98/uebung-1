//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2021.11.24 um 10:03:30 PM CET 
//


package dbt.protokoll.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA"
})
@XmlRootElement(name = "tagesordnungspunkt")
public class Tagesordnungspunkt {

    @XmlAttribute(name = "top-id")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String topId;
    @XmlElements({
        @XmlElement(name = "top-titel", type = TopTitel.class),
        @XmlElement(name = "name", type = Name.class),
        @XmlElement(name = "rede", type = Rede.class),
        @XmlElement(name = "p", type = P.class),
        @XmlElement(name = "zitat", type = Zitat.class),
        @XmlElement(name = "kommentar", type = Kommentar.class),
        @XmlElement(name = "a", type = A.class)
    })
    protected List<Object> topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA;

    /**
     * Ruft den Wert der topId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopId() {
        return topId;
    }

    /**
     * Legt den Wert der topId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTopId(String value) {
        this.topId = value;
    }

    /**
     * Gets the value of the topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTopTitelOrNameOrRedeOrPOrZitatOrKommentarOrA().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TopTitel }
     * {@link Name }
     * {@link Rede }
     * {@link P }
     * {@link Zitat }
     * {@link Kommentar }
     * {@link A }
     * 
     * 
     */
    public List<Object> getTopTitelOrNameOrRedeOrPOrZitatOrKommentarOrA() {
        if (topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA == null) {
            topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA = new ArrayList<Object>();
        }
        return this.topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA;
    }

    public List<Rede> getRedes(){
        return topTitelOrNameOrRedeOrPOrZitatOrKommentarOrA.stream().map(o -> o instanceof Rede? (Rede)o : null).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
