//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2021.11.24 um 10:03:30 PM CET 
//


package dbt.protokoll.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nameOrPOrKommentarOrZitatOrA"
})
@XmlRootElement(name = "rede")
public class Rede {

    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;
    @XmlAttribute(name = "redeart")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String redeart;
    @XmlElements({
        @XmlElement(name = "name", required = true, type = Name.class),
        @XmlElement(name = "p", required = true, type = P.class),
        @XmlElement(name = "kommentar", required = true, type = Kommentar.class),
        @XmlElement(name = "zitat", required = true, type = Zitat.class),
        @XmlElement(name = "a", required = true, type = A.class)
    })
    protected List<Object> nameOrPOrKommentarOrZitatOrA;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der redeart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedeart() {
        return redeart;
    }

    /**
     * Legt den Wert der redeart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedeart(String value) {
        this.redeart = value;
    }

    /**
     * Gets the value of the nameOrPOrKommentarOrZitatOrA property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameOrPOrKommentarOrZitatOrA property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameOrPOrKommentarOrZitatOrA().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Name }
     * {@link P }
     * {@link Kommentar }
     * {@link Zitat }
     * {@link A }
     * 
     * 
     */
    public List<Object> getNameOrPOrKommentarOrZitatOrA() {
        if (nameOrPOrKommentarOrZitatOrA == null) {
            nameOrPOrKommentarOrZitatOrA = new ArrayList<Object>();
        }
        return this.nameOrPOrKommentarOrZitatOrA;
    }

    public List<Name> getAllNames() {
        return nameOrPOrKommentarOrZitatOrA.stream().map(o -> o instanceof Name ? (Name) o : null).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<Kommentar> getKommentare() {
        return nameOrPOrKommentarOrZitatOrA.stream().map(o -> o instanceof Kommentar ? (Kommentar) o : null).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
