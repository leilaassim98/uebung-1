//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Anderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2021.11.24 um 10:03:30 PM CET 
//


package dbt.protokoll.domain;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "anlagenTitel",
    "anlage"
})
@XmlRootElement(name = "anlagen")
public class Anlagen {

    @XmlElement(name = "anlagen-titel")
    protected String anlagenTitel;
    @XmlElement(required = true)
    protected List<Anlage> anlage;

    /**
     * Ruft den Wert der anlagenTitel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnlagenTitel() {
        return anlagenTitel;
    }

    /**
     * Legt den Wert der anlagenTitel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnlagenTitel(String value) {
        this.anlagenTitel = value;
    }

    /**
     * Gets the value of the anlage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anlage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnlage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Anlage }
     * 
     * 
     */
    public List<Anlage> getAnlage() {
        if (anlage == null) {
            anlage = new ArrayList<Anlage>();
        }
        return this.anlage;
    }

}
