//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2021.11.24 um 10:03:30 PM CET 
//


package dbt.protokoll.domain;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "name")
public class Name {

    @XmlElement(required = true)
    protected String vorname;
    public String getvorname() {
        return vorname;
    }
    public void setvorname(String vorname) {
        this.vorname = vorname;
    }
    @XmlElement(required = true)
    protected String nachname;
    public String getnachname() {
        return nachname;
    }
    public void setnachname(String nachname) {
        this.nachname = nachname;
    }

    @XmlElement(required = true)
    protected String fraktion;
    public String getfraktion() {
        return fraktion;
    }
    public void setfraktion(String fraktion) {
        this.fraktion = fraktion;
    }

    @XmlMixed
    protected List<String> value;

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return vorname.equals(name.vorname) &&
                nachname.equals(name.nachname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vorname, nachname);
    }
}
