package MainPackage;
import dbt.protokoll.domain.*;
import java.util.*;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * MainClass is the main class of the application.
 * This maps all of the queries.
 */

public class MainClass {
    private List<DBT> dtdprotokoll;
    public MainClass(List<DBT> dbtplenarprotokolls) {
        this.dtdprotokoll = dbtplenarprotokolls;
    }


     // @return List of all speakers
    public List<Redner> AlleRedner() {
        List<List<Redner>> Redner = dtdprotokoll.stream().map(dbtplenarprotokoll -> dbtplenarprotokoll.getRednerliste().getRedner().stream().collect(Collectors.toList())).collect(Collectors.toList());
        return Redner.stream().flatMap(List::stream).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
    }



    /**
     * Filter implementation of the MPs
     * @param nameFilter
     * @return List of filtered speakers
     */
    public List<Redner> NamenFilter(String nameFilter) {
        List<Redner> nachVorname = AlleRedner().stream().filter(redner -> redner.getName().getvorname().contains(nameFilter)).collect(Collectors.toList());
        List<Redner> nachNachname = AlleRedner().stream().filter(redner -> redner.getName().getnachname().contains(nameFilter)).collect(Collectors.toList());
        nachNachname.addAll(nachVorname);
        return nachNachname;

    }


     // @return List of all speakers per party / parliamentary group
    public Map<String, List<Redner>> RednerProPartei() {
        Map<String, List<Redner>> RednerPartei = AlleRedner().stream()
                .collect(groupingBy(redner -> redner.getName().getfraktion() == null ?
                        "fraktionslos" : redner.getName().getfraktion()));
        return RednerPartei;
    }

    /**
     * Output of the text of an item on the agenda according to the index of meetings and numbers!
     * @param sessionIndex Session number/index
     * @param numberIndex Number of the agenda item
     * @return List of all objects in the agenda item
     */
    public List<Object> TextDesTagesordnungspunkte(int sessionIndex, int numberIndex) {
        Optional<DBT> sitzung = this.dtdprotokoll.stream()
                .filter(dbtplenarprotokoll -> dbtplenarprotokoll.getSitzungNr().contains(String.valueOf(sessionIndex))).findFirst();
        if (sitzung.isPresent()) {
            Optional<Tagesordnungspunkt> tagesordnungspunkt = sitzung.get().getSitzungsverlauf().getTagesordnungspunkt().stream().filter(tp -> tp.getTopId().contains(String.valueOf(numberIndex)))
                    .findFirst();
            if (tagesordnungspunkt.isPresent()) {
                return tagesordnungspunkt.get().getTopTitelOrNameOrRedeOrPOrZitatOrKommentarOrA();
            }
        }
        return null;
    }

    public List<Rede> AlleRede() {
        List<List<Tagesordnungspunkt>> dailyPointsTemp = this.dtdprotokoll.stream().map(dbtplenarprotokoll -> dbtplenarprotokoll.getSitzungsverlauf().getTagesordnungspunkt()).collect(toList());
        List<Tagesordnungspunkt> dailyPoints = dailyPointsTemp.stream().flatMap(List::stream).collect(toList());
        List<List<Rede>> speachesTemp = dailyPoints.stream().map(tagesordnungspunkt -> tagesordnungspunkt.getTopTitelOrNameOrRedeOrPOrZitatOrKommentarOrA().stream().map(o -> o instanceof Rede ? (Rede) o : null).collect(toList())).filter(redes -> redes != null).collect(toList());
        List<Rede> allSpeaches = speachesTemp.stream().flatMap(List::stream).filter(rede -> rede != null).collect(toList());
        return allSpeaches;
    }
}
