import MainPackage.MainClass;
import dbt.protokoll.domain.Redner;
import Interface.ImplInterfaceAnswers;
import Interface.ImplProtokollFiles;
import Interface.interfaces.InterfaceAnswers;
import Interface.interfaces.ProtokollFiles;
import Console.Menu;
//import Console.Ausgabe;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class XMLReader {

    public static void main(String[] args) {
        InterfaceAnswers protokollAnswerWriter = new ImplInterfaceAnswers();
        Menu ImplInterface = new Menu();
        ProtokollFiles ImplInterfaceAnswersDataBase = new ImplProtokollFiles();
        Ausgabe plenarRendererView = new Ausgabe();

        List<String> paths = new ArrayList<String>();
        for (int i = 1; i < 240; i++) {
            paths.add(String.format("bundestag/%s%s", i, ".xml"));
        }
        MainClass databaseQuery = ImplInterfaceAnswersDataBase.readFiles(paths).MainFiles();

        while(true){
            Menu.XMLReaderSelection selection = ImplInterface.mainMenu();

            switch (selection.getSelection()){
                case 1:
                    plenarRendererView.Redners(databaseQuery.AlleRedner());

                    Menu.XMLReaderSelection filterSelection = ImplInterface.filterNameMenu();
                    switch (filterSelection.getSelection()){
                        case 1:
                            plenarRendererView.Redners(databaseQuery.NamenFilter(filterSelection.getFilter()));
                            break;
                    }
                    break;
                case 2:
                    Map<String, List<Redner>> AlleRednerPrPartei = databaseQuery.RednerProPartei();
                    plenarRendererView.renderSpeakersPerParty(AlleRednerPrPartei);
                    break;
                case 3:
                    Menu.XMLReaderSelection sessionNumberAndIndexNumber = ImplInterface.selectSessionNumberAndIndexNumber();
                    List<Object> textOfDailySession = databaseQuery.TextDesTagesordnungspunkte(sessionNumberAndIndexNumber.getSessionIndex(), sessionNumberAndIndexNumber.getNumberIndex());
                    plenarRendererView.renderDailySessions(textOfDailySession);
                    break;


        }
        }
    }
}



