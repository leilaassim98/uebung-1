
import dbt.protokoll.domain.*;
import java.util.List;
import java.util.Map;


public class Ausgabe {

    public void Redners(List<Redner> speakers){
        speakers.stream().forEach(redner ->
                System.out.println(String.format("Name: %s %s, Party: %s", redner.getName().getnachname(), redner.getName().getvorname(), redner.getName().getfraktion())));
    }

    public void renderSpeakersPerParty(Map<String, List<Redner>> allSpeakersPerParty) {
        allSpeakersPerParty.forEach((key, value) -> {
            System.out.println("==================");
            System.out.println(key + ":");
            Redners(value);
        });
    }

    public void renderText(StringBuilder text){
        System.out.println(text.toString());
    }

    public void renderDailySessions(List<Object> textOfDailySession) {
        StringBuilder stringBuilder = new StringBuilder();
        textOfDailySession.stream().forEach(o -> {
            if(o instanceof TopTitel){
                stringBuilder.append(renderTopTitel((TopTitel) o));
            }else if(o instanceof Name){
                stringBuilder.append(renderName((Name) o));
            }else if(o instanceof Rede){
                stringBuilder.append(renderRede((Rede)o));
            }else if(o instanceof P){
                stringBuilder.append(renderP((P) o));
            }else if(o instanceof Zitat){
                stringBuilder.append(renderZitat((Zitat) o));
            }else if(o instanceof Kommentar){
                stringBuilder.append(renderKommentar((Kommentar) o));
            }else if(o instanceof A){
                stringBuilder.append(renderA((A) o));
            }
        });
        renderText(stringBuilder);
    }

    private String renderTopTitel(TopTitel o) {
        return o.getvalue() + "\n";
    }

    private String renderA(A o) {
        return o.getName() + "\n";
    }

    private String renderKommentar(Kommentar o) {
        return o.getvalue() + "\n";
    }

    private String renderZitat(Zitat o) {
        return o.getvalue() + "\n";
    }

    private String renderP(P o) {
        return o.getKlasse() + ", " + o.getvalue() + "\n";
    }

    private String renderRede(Rede o) {
        StringBuilder stringBuilder = new StringBuilder();
        o.getNameOrPOrKommentarOrZitatOrA().stream().forEach(r -> {
        if(r instanceof Name){
            stringBuilder.append(renderName((Name) r));
        }else if(r instanceof Rede){
            stringBuilder.append(renderRede((Rede)r));
        }else if(r instanceof P){
            stringBuilder.append(renderP((P) r));
        }else if(r instanceof Zitat){
            stringBuilder.append( renderZitat((Zitat) r));
        }else if(r instanceof Kommentar){
            stringBuilder.append(renderKommentar((Kommentar) r));
        }else if(r instanceof A){
            stringBuilder.append(renderA((A) r));
        } });
        return stringBuilder.toString();
    }

    private String renderName(Name o) {
        return String.format("Name: %s %s, Party: %s", o.getnachname(), o.getvorname(), o.getfraktion()) + "\n";
    }
}
