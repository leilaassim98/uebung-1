package Console;
import java.util.Scanner;

//Menu
public class Menu {
    Scanner input = new Scanner(System.in);

    public XMLReaderSelection mainMenu(){
        int selection;
        /***************************************************/

        System.out.println("**************************************Men�************************++++++**************");
        System.out.printf("%n");
        System.out.println("*  (1)* Auflistung aller Redner*innen inkl. Partei- / Fraktionszugeh�rigkeit            *");
        System.out.println("*  (2)* Auflistung aller Redner pro Partei / Fraktion.                                  *");
        System.out.println("*  (3)* Ausgabe des Textes eines Tagesordnungspunktes gem�� Sitzungs- und Nummern-Index *");
        System.out.println("(0)* Exit ");
        System.out.printf("%n");
        System.out.println("**********************************************************************++++++*************");




        selection = input.nextInt();
        return XMLReaderSelection.build().withSelection(selection);
    }

    public XMLReaderSelection filterNameMenu() {
        int selection = 0;
        String filter = "";
        System.out.printf("%n%n");

        System.out.println("W�hlen Sie Entweder");
        System.out.printf("%n");
        System.out.println("(1)* Filter f�r die Namen der Abgeordneten");

        System.out.println("(0)* - Back");

        selection = input.nextInt();
        if(selection == 1){
            System.out.println("Please filter for name:");
            input.nextLine();// this is needed as scanner just miss the input on console
            filter =input.nextLine();
        }

        return XMLReaderSelection.build().withSelection(selection).withFilter(filter);
    }

    //System.out.println("3 - Output of the text of an agenda item according to the session and number index. Q2f3");
    public XMLReaderSelection selectSessionNumberAndIndexNumber() {
        int selection = 0;
        int sessionIndex = 0;
        int numberIndex = 0;

        System.out.println("Choose from these choices");
        System.out.println("-------------------------\n");
        System.out.println("(1)* Sitzungs- und Nummern-Index eigeben");

        System.out.println("*  0 - Back");

        selection = input.nextInt();
        if(selection == 1){
            System.out.println("**Bitte Sitzung-index eigeben ");
            input.nextLine();
            sessionIndex = input.nextInt();

            System.out.println("*Bitte Nummern-Indx eingeben:");
            input.nextLine();
            numberIndex =input.nextInt();
        }
        return XMLReaderSelection.build().withSessionIndex(sessionIndex).withNumberIndex(numberIndex);
    }



    public static class XMLReaderSelection {
        private int selection;
        private String filter;
        private int sessionIndex;
        private int numberIndex;

        private XMLReaderSelection() {
        }

        public int getSessionIndex() {
            return sessionIndex;
        }

        private void setSessionIndex(int sessionIndex) {
            this.sessionIndex = sessionIndex;
        }

        public int getNumberIndex() {
            return numberIndex;
        }

        private void setNumberIndex(int numberIndex) {
            this.numberIndex = numberIndex;
        }

        private void setSelection(int selection) {
            this.selection = selection;
        }

        private void setFilter(String filter) {
            this.filter = filter;
        }

        public int getSelection() {
            return selection;
        }

        public String getFilter() {
            return filter;
        }

        public static XMLReaderSelection build(){
            XMLReaderSelection plenarReaderViewSelection = new XMLReaderSelection();
            return plenarReaderViewSelection;
        }

        public XMLReaderSelection withSelection(int selection){
            this.setSelection(selection);
            return this;
        }

        public XMLReaderSelection withFilter(String filter){
            this.setFilter(filter);
            return this;
        }

        public XMLReaderSelection withSessionIndex(int sessionIndex) {
            this.setSessionIndex(sessionIndex);
            return this;
        }

        public XMLReaderSelection withNumberIndex(int numberIndex) {
            this.setNumberIndex(numberIndex);
            return this;
        }
    }
}
